import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

//import the "useNavigate" hook from react-router-dom and redirect the user back to the "Courses" page after enrolling to a course.

export default function ProductView(){

	//Consume the "User" context object to be able to obtain the user ID so we can enroll a user
	const { user } = useContext(UserContext);

	// Allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course
	const navigate = useNavigate();

	//Retrieve the "courseId" via the url using the "useParams" hook from react-router-dom and create a "useEffect" hook to check if the courseId is retrieved properly
	const { productId, quantity } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);


	//"order" function that will order a user to a specific course and bind it to the "Order" button
	const order = (productId, quantity) =>{

		// `${process.env.REACT_APP_API_URL}/orders`

		// https://ecommerce-cs2-abad-v3.onrender.com/orders

		fetch(`https://ecommerce-cs2-abad-v3.onrender.com/orders`,{
			method:'POST',
			headers:{
				'Content-Type':'application/json',
				Authorization:`Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res=>res.json())
		.then(data=>{
			//conditional statement that will alert the user of a successful/failed enrollment
			console.log(data)
			if(data === true && user.isAdmin === false){

				Swal.fire({
				  title: "Order Successful!",
				  icon: "success",
				  text: "-"
				});

				console.log(order)
				// redirect the user to a different page and is an easier approach
				navigate("/products")

			}else{

				Swal.fire({
				  title: "Something went wrong!",
				  icon: "error",
				  text: "Admins not allowed to check out orders!"
				});

			}

		})
	}

	useEffect(()=>{
		//fetch request that will retrieve the details of the product from our database to be displayed in the "ProductView" page
		console.log(productId);

		// `${process.env.REACT_APP_API_URL}/products/${productId}`


		// http://localhost:4000/products/6368e7c9e600b1d3fd90e89d

		// https://ecommerce-cs2-abad-v3.onrender.com/products/${productId}

		fetch(`https://ecommerce-cs2-abad-v3.onrender.com/products/${productId}`)
		.then(res=>res.json())
		.then(data=>{
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	},[productId, quantity])

	return(
		<Container className="mt-5">
		  <Row>
		     <Col lg={{span:6, offset:3}}>
				<Card>
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>PHP {price}</Card.Text>
						{/*conditionally render the enroll button if a user is logged in and a button that will redirect the a user to the "Login" page if they are not logged in*/}
						{
							(user.id!==null)?
							<>
							<Button className = "m-2" variant="primary" onClick={()=>order(productId, quantity)}>Order</Button>
							<Button className = "m-2" as={Link} to="/products"variant="primary">Return to List</Button>
							</>
							:
							<Link className="btn btn-danger" to="/login">Log in to Order</Link>
						}
						
					</Card.Body>
				</Card>
		     </Col>
		  </Row>
		</Container>
	)
}