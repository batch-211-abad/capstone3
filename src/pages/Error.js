import Banner from '../components/Banner.js'

export default function Error(){

	const data = {
		title: "404 - Not found",
		content: "The page you are looking cannot be found.",
		destination: "/",
		label: "Return to Homepage"
	}

	return(
		// "prop" name is up to the developer
		<Banner bannerProp = {data}/>
		)
}