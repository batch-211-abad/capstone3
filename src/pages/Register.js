// import Button from 'react-bootstrap/Button';
// import Form from 'react-bootstrap/Form';

import { Form, Button } from 'react-bootstrap'

//S54 ACTIVITY - Conditional Rendering if user is logged-in, /reigster should not accessible, will navigate to /courses instead
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

/*
  Mini Activity - add input fields for firstName, lastName, and mobileNo
*/

//Define state hooks for all input fields and an "isActive" state for conditional rendering of the submit btn

export default function Register() {

  // Allows us to consume the User Context/Data and its properties for validation
  const { user } = useContext(UserContext);

  const navigate = useNavigate();

    //create state hooks to store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password, setPassword] = useState('');
    const [password2, setPassword2] = useState('');

    //create a state to determine whether the submit button is enabled or not

    const [isActive, setIsActive] = useState(false);

    console.log(firstName);
    console.log(lastName);
    console.log(email);
    console.log(mobileNo);
    console.log(password);
    console.log(password2);

    // Function to simulate user registration
    function registerUser(e){

      // prevents page redirection via form submission
      e.preventDefault();

      //fetch request that will access the checkEmail route and confirm if the user's email is still available and invoke the setter functions for the other input fields to clear the form

      // fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`,{

      // fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`,{
      
      // https://ecommerce-cs2-abad-v3.onrender.com/users/checkEmail
      fetch(`https://ecommerce-cs2-abad-v3.onrender.com/users/checkEmail`,{
        method: "POST",
        headers:{
          'Content-Type':'application/json'
        },
        body: JSON.stringify({
          email: email
        })
      })
      .then(res=>res.json())
      .then(data=>{
        console.log(data)

        //conditional statement that will alert the user if the email provided is already taken and the registration was unsuccessful

        if(data === true){

            Swal.fire({
            title: "Duplicate Email Found!",
            icon: "error",
            text: "Kindly provide another email to complete the registration!"
          })

        }else{


          // fetch(`${process.env.REACT_APP_API_URL}/users/register`,{

          // https://ecommerce-cs2-abad-v3.onrender.com/users/register

          fetch(`https://ecommerce-cs2-abad-v3.onrender.com/users/register`,{
            method:"POST",
            headers:{
              'Content-Type':'application/json'
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              password: password,
              mobileNo: mobileNo
            })
          })
          .then(res=>res.json())
          .then(data=>{

            //conditional statement that will access the "/register" route if no duplicate email is found, clear the form and alert the user of successful/failed registration

            if(data===true){

              setFirstName("");
              setLastName("");
              setEmail("");
              setMobileNo("");
              setPassword("");
              setPassword2("");

              Swal.fire({
                title: "Registration successful!",
                icon: "success",
                text: "You may now start logging in."
              })

              //redirect to the Login page upon successful registration
              navigate("/login")

            }else{

              Swal.fire({
                title: "Something went wrong!",
                icon: "error",
                text: "Please try again!"

            })



          }
        })
      }
    })

      // alert("Thank you for registering!")
  }

    /*
        Two Way Binding
        -it is done so that we can assure that we can save the input into our states as we type into the input elements. This is done so what we don't have to save it just before we submit

        e.target - current element where the event happens
        e.target.value - current value of the element where the event happened


    */


  useEffect(()=>{

      // Enable the submit button if:
      // All the fields are populated.
      // both passwords match.

    if((firstName !== "" && lastName !== "" && mobileNo.length === 11 && email !=="" && password !== "" && password2 !== "") && (password===password2)){
      setIsActive(true)
    }else{
      setIsActive(false)
    }

  },[firstName,lastName,email,mobileNo,password,password2])




  return (
    //S54 Activity continuation of solution code
    // Conditional Rendering
    // LOGIC - if there is a user logged-in in the web application, enpoint or "/register" should not be accesible. The user should be navigated to courses tab instead.
    //we use the id of the user for redirecting the user back to the appropriate page
    (user.id !== null)
    ?
    <Navigate to="/login"/>
    // ELSE - if the localStorage is empty, the user is allowed to access the login page.
    :
    <Form onSubmit={(e)=>registerUser(e)}>
      <Form.Group className="mb-3" controlId="firstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
        type="text"
        placeholder="Enter First Name"
        value={firstName}
        onChange={e=>setFirstName(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="lastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
        type="text"
        placeholder="Enter Last Name"
        value={lastName}
        onChange={e=>setLastName(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="email">
        <Form.Label>Email address</Form.Label>
        <Form.Control
        type="email"
        placeholder="Enter email"
        value={email}
        onChange={e=>setEmail(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="mobileNo">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
        type="text"
        placeholder="Enter Mobile Number"
        value={mobileNo}
        onChange={e=>setMobileNo(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
        type="password"
        placeholder="Password"
        value={password}
        onChange={e=>setPassword(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2">
        <Form.Label> Verify Password</Form.Label>
        <Form.Control
        type="password"
        placeholder="Verify Password"
        value={password2}
        onChange={e=>setPassword2(e.target.value)}
        required/>
      </Form.Group>


        {/*Conditional rendering - submit button will be active based on the isActive state*/}
        { isActive ?      
            <Button variant="success" type="submit" id="submitBtn">
                Submit
            </Button>
          :
            <Button variant="danger" type="submit" id="submitBtn" disabled>
                Submit
            </Button>
        }

    </Form>
  );
}